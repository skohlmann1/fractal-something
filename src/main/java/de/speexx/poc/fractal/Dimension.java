/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

public record Dimension(int rows, int columns, int interpolation) {
    
    public Dimension {
        if (rows < 1 || columns < 1 || interpolation < 1) {
            throw new IllegalArgumentException(String.format("No value must < 1. rows: %d - columns: %d - interpolation: %d", rows, columns, interpolation));
        }
    }

    public Dimension(final int rows, final int columns) {
        this(rows, columns, 1);
    }
    
    public int interpolatedRows() {
        return this.rows * this.interpolation;
    }
    
    public int interpolatedColumns() {
        return this.columns * this.interpolation;
    }
    
    public boolean isInterpolated() {
        return this.interpolation != 1;
    }
}
