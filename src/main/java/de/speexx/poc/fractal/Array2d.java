/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

import java.lang.reflect.Array;

/**
 *
 * @author saschakohlmann
 */
public final class Array2d<T> {
    
    private final Dimension dimension;
    private final int width;
    private final int height;
    private final T[] array;
    
    private Array2d(final Class<T> clazz, final Dimension dimension) {
        this.dimension = dimension;
        this.width = dimension.interpolatedColumns();
        this.height = dimension.interpolatedRows();
        this.array = (T[]) Array.newInstance(clazz, this.width * this.height);
    }
    
    public static final <T> Array2d<T> newInstance(final T type, final Dimension dimension) {
        return new Array2d(type.getClass(), dimension);
    }

    public static final <T> Array2d<T> newInstance(final Class<T> clazz, final Dimension dimension) {
        return new Array2d(clazz, dimension);
    }
    
    public void set(final int x, final int y, final T value) {
        this.array[y * this.width + x] = value;
    }

    public T get(final int x, final int y) {
        return this.array[y * this.width + x];
    }
    
    public int width() {
        return this.width;
    }

    public int height() {
        return this.height;
    }
    
    public Dimension dimension() {
        return this.dimension;
    }
}
