/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

import java.awt.Color;
import static java.awt.Color.WHITE;
import static java.awt.Color.BLACK;

import static de.speexx.poc.fractal.Assertions.assertRange;
import static de.speexx.poc.fractal.Assertions.assertNotNull;
import static de.speexx.poc.fractal.Assertions.assertSize;
import static de.speexx.poc.fractal.Assertions.assertContainsNoNull;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import static java.lang.String.format;
import java.util.stream.IntStream;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import static java.lang.StrictMath.PI;
import static java.lang.StrictMath.ceil;

/**
 *
 * @author saschakohlmann
 */
public class ImagePlotter3D extends ImagePlotter {
    
    int maximumIterations;
    final Color[] colors;
    final Color maximumIterationColor;
    final Color boundrayColor;
    
    public ImagePlotter3D(final int maximumIterations) {
        this(maximumIterations, new Color[] {BLACK, WHITE}, Color.BLACK, Color.WHITE);
    }

    private ImagePlotter3D(final int maximumIterations, final Color[] colors, final Color maximumIterationColor, final Color boundrayColor) {
        this.maximumIterations = assertRange(maximumIterations, 1, Integer.MAX_VALUE, () -> format("maximum iterations must be in range from %d to %d. Is: %d", 0, Integer.MAX_VALUE, maximumIterations));
        this.maximumIterationColor = assertNotNull(maximumIterationColor, () -> format("Maximum iteration color must not be null"));
        this.boundrayColor = assertNotNull(boundrayColor, () -> format("Boundray color must not be null"));
        final var colorList = Arrays.asList(colors);
        assertContainsNoNull(colorList, () -> format("Gradient color must not contain null value"));
        assertSize(colorList, 2, 2, () -> format("Gradient color amount must contain %d colors. Is: %d", 2, colors.length));
        this.colors = colorList.toArray(new Color[colors.length]);
    }

    public BufferedImage draw(final Array2d<FractalValue> data) {
        
        final var defaultColor = this.maximumIterationColor;
        final var interpolation = data.dimension().interpolation();
        
        // Color table with separate red, green and blue values of the gradient is faster in calculation
        // Length of array otimized for "Determine gradient color". See there below
        final var gradientColorsRed =  new double[this.colors.length][2];
        final var gradientColorsGreen =  new double[this.colors.length][2];
        final var gradientColorsBlue =  new double[this.colors.length][2];
        IntStream.range(0, this.colors.length-1).forEach(idx -> {
            gradientColorsRed[idx][0] = (double) this.colors[idx].getRed();
            gradientColorsRed[idx][1] = (double) this.colors[idx+1].getRed();
            gradientColorsGreen[idx][0] = (double) this.colors[idx].getGreen();
            gradientColorsGreen[idx][1] = (double) this.colors[idx+1].getGreen();
            gradientColorsBlue[idx][0] = (double) this.colors[idx].getBlue();
            gradientColorsBlue[idx][1] = (double) this.colors[idx+1].getBlue();
        });

        final var defaultColorRed = defaultColor.getRed();
        final var defaultColorGreen = defaultColor.getGreen();
        final var defaultColorBlue = defaultColor.getBlue();
        
        final var h2 = 1.5;
        final var angle = 45;
        final var v = Complex.ONE.multiply(angle * 2 * PI / 360).exponential();
        final var i = 0;
        // Finish precalculation

        final var bufferedImage = new BufferedImage(data.dimension().columns(), data.dimension().rows(), TYPE_INT_ARGB);
        IntStream.range(0, data.dimension().columns()).parallel().forEach(x -> {
            IntStream.range(0, data.dimension().rows()).forEach(y -> {
                var red = 0.0;
                var green = 0.0;
                var blue = 0.0;
                var alpha = 0.0;
                var pixelCount = 0.0; // Interpolation Count

                for (int xx = 0; xx < interpolation; xx++) {
                    final var xxx = x * interpolation + xx;
                    for (int yy = 0; yy < interpolation; yy++){
                        pixelCount++;
                        final var yyy = y * interpolation + yy;
                        final var value = data.get(xxx, yyy);

                        final var iterations = value.iterations();
                        if (iterations < this.maximumIterations) {
                            final var u = value.distance().divide(value.derivate());
                            final var uAbs = u.divide(u.abs());
                            final var t = uAbs.real() * v.real() + uAbs.imaginary() + v.imaginary() + h2; // dot product
                            final var ts = t / ( 1 + h2);
                            final var f = ts < 0 ? 0 : ts;
                            
                            // Calculate gradient
                            red = red + (gradientColorsRed[i][0] + (gradientColorsRed[i][1] - gradientColorsRed[i][0]) * f);
                            green = green + (gradientColorsGreen[i][0] + (gradientColorsGreen[i][1] - gradientColorsGreen[i][0]) * f);
                            blue = blue + (gradientColorsBlue[i][0] + (gradientColorsBlue[i][1] - gradientColorsBlue[i][0]) * f);

                            if (true) {
                                alpha = alpha + (255 + 0 - 255 * f) + 128;
                            } else {
                                alpha = 255;
                            }
                            
                        } else {
                            red = (double) defaultColorRed;
                            green = (double) defaultColorGreen;
                            blue = (double) defaultColorBlue;
                            alpha = 255;
                        }
                    }
                }
                
                // Finish interpolation
                final var redi = (int) ceil(red / pixelCount);
                final var greeni = (int) ceil(green / pixelCount);
                final var bluei = (int) ceil(blue / pixelCount);
                final var alphai = (int) ceil(alpha / pixelCount);
                
                // Gradient calculates values less than 0 and greater than 255. These values must be adjusted from 0 and 255 respectively.
                bufferedImage.setRGB(x, y, new Color(redi < 0 ? 0 : redi > 255 ? 255 : redi, greeni < 0 ? 0 : greeni > 255 ? 255 : greeni,  bluei < 0 ? 0 : bluei > 255 ? 255 : bluei, alphai < 0 ? 0 : alphai > 255 ? 255 : alphai).getRGB());
            });
        });
        
        
        return bufferedImage;
    }

    public final ImagePlotter3D withMaximumIterationColor(final Color maxIterationColor) {
        return new ImagePlotter3D(this.maximumIterations, this.colors, maxIterationColor, this.boundrayColor);
    }

    public final ImagePlotter3D withMaximumIterations(final int maxIterations) {
        return new ImagePlotter3D(maxIterations, this.colors, this.maximumIterationColor, this.boundrayColor);
    }

//    public final ImagePlotter3D withBoundrayColor(final Color boundrayColor) {
//        return new ImagePlotter3D(this.maximumIterations, this.colors, this.maximumIterationColor, this.boundrayColor);
//    }

    public final ImagePlotter3D withColors(final Color[] cols) {
        return new ImagePlotter3D(this.maximumIterations, cols, this.maximumIterationColor, this.boundrayColor);
    }


    public final int maximumIterations() {
        return this.maximumIterations;
    }

    public final Color maximumIterationColor() {
        return this.maximumIterationColor;
    }

//    public final Color boundrayColor() {
//        return this.boundrayColor;
//    }

    public final Color[] colors() {
        return Arrays.copyOf(this.colors, this.colors.length);
    }
}
    
