/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

/**
 *
 * @author saschakohlmann
 */
record PlotWindow(int maxIterations, Complex start, Complex end) {

    public final static PlotWindow[] PLOT_WINDOWS = new PlotWindow[] {
        new PlotWindow(200, new Complex(-2.2, -1.2), new Complex(1, 1.2)),  // Base Mandelbrot, 4:3

        new PlotWindow(300, new Complex(-1.79275, -0.020000000000000018), new Complex(-1.7360833333333334, 0.022499999999999964)),
        new PlotWindow(200, new Complex(-1.759064814814815, 0.01885916666666663), new Complex(-1.7586240740740742, 0.019189722222222184)),
        new PlotWindow(350, new Complex(-1.758657068415638, 0.019056765432098728), new Complex(-1.7586355210905351, 0.01907292592592589)),
        new PlotWindow(400, new Complex(-0.1216, -0.9375), new Complex(-0.0883, -0.9125)), // move 1.1
        new PlotWindow(400, new Complex(-0.104638, -0.9357), new Complex(-0.102972, -0.934527)), // move 1.2
        new PlotWindow(500, new Complex(-0.10383240740740739, -0.9352306944444444), new Complex(-0.10381203703703704, -0.9352154166666666)), // move 1.3
        new PlotWindow(500, new Complex(-0.10381323662551435, -0.9352300833333332), new Complex(-0.10381273868312763, -0.9352297098765432)), // move 1.4
        new PlotWindow(500, new Complex(-0.10381282658379058, -0.9352300308834018), new Complex(-0.10381281219878837, -0.9352300200946502)), // move 1.5
        new PlotWindow(700, new Complex(-0.10381281461307129, -0.9352300261015873), new Complex(-0.10381281446922118, -0.9352300259936999)), // move 1.6
        new PlotWindow(700, new Complex(-0.10381281455690583, -0.9352300260518392), new Complex(-0.10381281455562714, -0.9352300260508802)), // move 1.7
        new PlotWindow(800, new Complex(-0.10381281455621326, -0.9352300260513715), new Complex(-0.10381281455615775, -0.9352300260513299)), // move 1.8 - unscharf
        new PlotWindow(800, new Complex(-0.10381281455617274, -0.9352300260513561), new Complex(-0.10381281455616853, -0.9352300260513529)), // move 1.9 - unpräziser pixelmatsch
        new PlotWindow(800, new Complex(-0.10381281455617002, -0.9352300260513542), new Complex(-0.1038128145561697, -0.935230026051354)), // move 1.10 - 3 horizontale bereich
        new PlotWindow(800, new Complex(-0.1038128145561701, -0.935230026051355), new Complex(-0.10381281455617, -0.93523002605136)), // move 10 - farbmuster
        new PlotWindow(450, new Complex(-0.10381281455617, -0.93523002605135), new Complex(-0.10381281455616, -0.9352300260513)), // An der IEEE 754 Grenze

        new PlotWindow(3000, new Complex(-0.752431611111111, 0.03401611111111098), new Complex(-0.7514267222222221, 0.03476977777777772)), // move 2.1
        new PlotWindow(3000, new Complex(-0.7522381699999999, 0.03452033504629619), new Complex(-0.752123445185185, 0.03460637865740736)), // move 2.2
        new PlotWindow(3000, new Complex(-0.752213251451542, 0.03455543414734867), new Complex(-0.7521651651827724, 0.034591498848925886)), // move 2.3
        new PlotWindow(5000, new Complex(-0.7521991060741456, 0.034565011329211956), new Complex(-0.7521761849526989, 0.034582202170297095)), // move 2.4  (Spirale)
        new PlotWindow(6000, new Complex(-0.7521887872701494, 0.034573543070634496), new Complex(-0.7521887823947105, 0.03457354672721365)), // Bunt und wirr 3.1
        new PlotWindow(20000, new Complex(-0.752188786132547, 0.03457354498373639), new Complex(-0.752188785649066, 0.03457354534634716)), // Bunt und wirr 3.2
        new PlotWindow(20000, new Complex(-0.7521887859016123, 0.03457354514646804), new Complex(-0.7521887859012255, 0.03457354514675813)), // Eingang zu spirale mit Doppelspirale am Ende 3.3
        new PlotWindow(20000, new Complex(-0.7521887859014392, 0.03457354514659176), new Complex(-0.7521887859014111, 0.03457354514661282)), // Doppelspirale am Ende mit Beginn IEEE 754 Problemen 3.4

        new PlotWindow(400, new Complex(-1.7832681547006408, -0.000007509833925688394), new Complex(-1.7832489342027666, 0.000006905539479941201)), //Kreuz
        
        new PlotWindow(800, new Complex(-1.0677499999999998, 0.257125), new Complex(-1.0502499999999997, 0.27025000000000005)), // Schneeflocke
        new PlotWindow(2400, new Complex(0.18338915131575798, -0.5959989941589506), new Complex(0.18338915202117637, -0.5959989936298868)), // Schneeflocke

        new PlotWindow(300, new Complex(-0.0081239025138, -0.6548059906435), new Complex(-0.0061239025138, -0.5548059906435)), // Schneeflocke
    };

    public PlotWindow adjustAspectRatio(final Dimension dimension) {

//        final var dimensionRation = (double) dimension.columns() / (double) dimension.rows();
//        final var objectRatio = (end().real() - start().real()) / (end().imaginary() - start().imaginary());
//
//        if (dimensionRation > objectRatio) {
//            final var delta = ((end().imaginary() - start().imaginary()) * dimensionRation - (end().real() - start().real())) / 2;
//            System.out.format("dimensionRation > objectRatio: dimensionRation: %f - objectRatio: %f - delta: %f%n", dimensionRation, objectRatio, delta);
//            return new PlotWindow(this.maxIterations(),
//                                  new Complex(start().real() - delta, start().imaginary()),
//                                  new Complex(end().real() + delta, end().imaginary()));
//        } else if (objectRatio > dimensionRation) {
//            final var delta = ((end().real() - start().real()) * dimensionRation - (end().imaginary() - start().imaginary())) / 2;
//            System.out.format("objectRatio > dimensionRation: dimensionRation: %f - objectRatio: %f - delta: %f%n", dimensionRation, objectRatio, delta);
//            return new PlotWindow(this.maxIterations(),
//                                  new Complex(start().real(), start().imaginary() - delta),
//                                  new Complex(end().real(), end().imaginary() + delta));
//        }
//        
//        System.out.format("objectRatio == dimensionRation: dimensionRation: %f - objectRatio: %f%n", dimensionRation, objectRatio);
        return this;
    }
}
