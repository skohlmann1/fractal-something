/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

import java.awt.Color;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import java.io.FileOutputStream;
import static java.lang.StrictMath.PI;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import static java.lang.StrictMath.cos;

/**
 *
 * @author saschakohlmann
 */
public class Gradient {

//    final static Color[] COLORS = new Color[] {new Color(20, 20, 20), Color.BLUE, Colors.YELLOW, new Color(21, 21, 21)};
//    final static Color[] COLORS = new Color[] {Colors.MEDIUMGREEN, Colors.WHITE};
//    final static Color[] COLORS = new Color[] {Colors.BLACK, Colors.WHITE};
//    final static Color[] COLORS = new Color[] {Colors.RED, Colors.GREEN, Colors.BLUE, Colors.RED};
//    final static Color[] COLORS = new Color[] {Colors.CYAN, Colors.MAGENTA, Colors.YELLOW, Colors.DARK_GRAY.darker()};
//    final static Color[] COLORS = new Color[] {Colors.CYAN, Colors.MAGENTA, Colors.YELLOW, Colors.DARK_GRAY.darker(), Colors.CYAN};
//    final static Color[] COLORS = new Color[] {Colors.NAVYDARKBLUE.darker(), Colors.BLUE, Colors.WHITE, Colors.WHITE};
//    final static Color[] COLORS = new Color[] {new Color(0, 0x80, 0), Colors.WHITE, new Color(0xff, 0, 0x80)};
//    final static Color[] COLORS = new Color[] {new Color(0, 0x80, 0), Colors.WHITE, new Color(0xff, 0, 0x80), new Color(0, 0x80, 0)};
//    final static Color[] COLORS = new Color[] {Colors.MAGENTA, Colors.LIGHT_GRAY, Colors.GREEN, Colors.MAGENTA};
//    final static Color[] COLORS = new Color[] {Colors.DARKGREEN, Colors.LIGHT_GRAY, Colors.YELLOW};
//    final static Color[] COLORS = new Color[] {Colors.VERYDARKRED, Colors.YELLOW, Colors.ORANGE};
//    final static Color[] COLORS = new Color[] {Colors.BLACK, Colors.RED, Colors.ORANGE, Colors.YELLOW, Colors.WHITE};
//    final static Color[] COLORS = new Color[] {Colors.DARKRED, Colors.RED, Colors.ORANGE, Colors.YELLOW, Colors.WHITE};
//    final static Color[] COLORS = new Color[] {Colors.VERYDARKGREEN, Colors.DARKGREEN, Colors.LEAFGREEN, Colors.YELLOW, Colors.WHITE};
//    final static Color[] COLORS = new Color[] {Colors.BLACK, Colors.MAGENTA, Colors.ELECTRICBLUE, Colors.LIGHT_GRAY, Colors.YELLOW, Colors.ORANGE, Colors.BLACK};
//    final static Color[] COLORS = new Color[] {Colors.VERYDARKGRAY, Colors.NAVYDARKBLUE, Colors.ELECTRICBLUE, Colors.LIGHT_GRAY, Colors.YELLOW, Colors.ORANGE, Colors.VERYDARKGRAY};
    final static Color[] COLORS = new Color[] {Colors.BLACK, Colors.VERYDARKGRAY, Colors.DARK_GRAY, Colors.GRAY, Colors.LIGHT_GRAY, Colors.WHITE};
//    final static Color[] COLORS = new Color[] {Colors.VERYDARKGRAY, Colors.ORANGE, Colors.YELLOW, Colors.LIGHT_GRAY, Colors.ELECTRICBLUE, Colors.NAVYDARKBLUE, Colors.VERYDARKGRAY};
//    final static Color[] COLORS = new Color[] {Colors.NAVYDARKBLUE, Colors.BLUE, Colors.ELECTRICBLUE, Colors.LIGHT_GRAY, Colors.YELLOW, Colors.DARKGREEN, Colors.VERYDARKGREEN};
//    final static Color[] COLORS = new Color[] {Colors.BLACK, Colors.MAGENTA, Colors.ELECTRICBLUE, Colors.LIGHT_GRAY, Colors.YELLOW, Colors.ORANGE, Colors.RED, Colors.BLACK};
//    final static Color[] COLORS = new Color[] {Colors.BLACK, Colors.RED, Colors.ORANGE, Colors.YELLOW, Colors.LIGHT_GRAY, Colors.ELECTRICBLUE, Colors.MAGENTA, Colors.BLACK};
//    final static Color[] COLORS = new Color[] {Colors.DARK_GRAY, Colors.LIGHTBLUE, Colors.ELECTRICBLUE, Colors.LIGHT_GRAY, Colors.YELLOW, Colors.GREEN, Colors.ORANGE, Colors.DARK_GRAY};
//    final static Color[] COLORS = new Color[] {Colors.NAVYDARKBLUE, Colors.VERYDARKGREEN, Colors.DARKGREEN, Colors.ORANGE, Colors.BLACK};
//    final static Color[] COLORS = new Color[] {Colors.NAVYDARKBLUE, Colors.YELLOW};
//    final static Color[] COLORS = new Color[] {Colors.NAVYDARKBLUE, Colors.ELECTRICBLUE};
//    final static Color[] COLORS = new Color[] {Colors.NAVYDARKBLUE, Colors.ELECTRICBLUE, Colors.ORANGE, Colors.YELLOW, Colors.NAVYDARKBLUE};
//    final static Color[] COLORS = new Color[] {Colors.NAVYDARKBLUE, Colors.YELLOW, Colors.ELECTRICBLUE, Colors.ORANGE, Colors.NAVYDARKBLUE};
//    final static Color[] COLORS = new Color[] {Colors.YELLOW, Colors.NAVYDARKBLUE, Colors.BLACK};
//    final static Color[] COLORS = new Color[] {Colors.YELLOW, Colors.NAVYDARKBLUE, Colors.BLACK, Colors.YELLOW};
//    final static Color[] COLORS = new Color[] {Colors.CANNIEST_SEPIA, Colors.ILIAC_BRIGHT_VIOLET, Colors.DELIGENT_LIGHT_NAVY};

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        
//        final var gradient = gradient(400, Color.BLUE, Color.BLACK, Color.YELLOW, Color.MAGENTA, Color.GREEN, Color.WHITE, Color.ORANGE);
        final var gradient = new Gradient().calculate(400, true, Color.BLACK, Color.MAGENTA, Colors.ELECTRICBLUE, Color.LIGHT_GRAY, Color.YELLOW, Color.ORANGE, Color.RED, Color.BLACK);
//        final var gradient = gradient(400, Color.BLUE, Color.BLACK, Color.GREEN);
//        final var gradient = gradient(400, true, Color.BLUE, Color.YELLOW, Color.WHITE, Color.GREEN);
        final var bufferedImage = new BufferedImage(gradient.size(), 100, TYPE_INT_ARGB);
        for (int x = 0; x < gradient.size(); x++) {
            
            final var color = gradient.get(x);
            for (int y = 0; y < 100; y++) {
                bufferedImage.setRGB(x, y, color.getRGB());
            }
        }
        try (final var output = new FileOutputStream("/Users/saschakohlmann/gradient.png")) {
            ImageIO.write(bufferedImage, "PNG", output);
        }
    }
    
    public final List<Color> calculate(final int width, final boolean debanding, final Color... sourceColors) {
        final var perTransitionSteps = (int) StrictMath.floor((double) width / (double) sourceColors.length);
        final var totalColors = perTransitionSteps * sourceColors.length - perTransitionSteps;
        final var gradient = new ArrayList<Color>((int) totalColors);
        
        for (int j = 0;  j < sourceColors.length - 1; j++) {
            final var color1 = sourceColors[j];
            final var color2 = sourceColors[j + 1];
            for (int i = 0; i < perTransitionSteps; i++) {
                final var inter = (double) i / (double) perTransitionSteps;
                final var ratio = debanding ? (cos(inter * PI + PI) + 1) / 2 : inter;
                final var red = (int) (color2.getRed() * ratio + color1.getRed() * (1 - ratio));
                final var green = (int) (color2.getGreen() * ratio + color1.getGreen() * (1 - ratio));
                final var blue = (int) (color2.getBlue() * ratio + color1.getBlue() * (1 - ratio));
                final var alpha = (int) (color2.getAlpha()* ratio + color1.getAlpha() * (1 - ratio));
                
                final var stepColor = new Color(red, green, blue, alpha);
                gradient.add(stepColor);
            }
        }
        return gradient;
    }
}
