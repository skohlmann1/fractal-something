/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

/**
 *
 * @author saschakohlmann
 */
public final class NewtonFractal implements Formular {
    
    private final double tolerance;
    private final int maxIterations;
 
    public static NewtonFractal newInstance() {
        return new NewtonFractal(1e-8, 300);
    }
    
    private NewtonFractal(final double tolerance, final int maxIterations) {
        this.maxIterations = maxIterations;
        this.tolerance = tolerance;
    }
    
    @Override
    public FractalValue calculate(final Complex c) {
        
        var z = c;
        var z1 = Complex.ZERO;
        var derivate = Complex.ONE;
        var reason = Reason.NOT_ENOUGH_ITERATES;
        var n = 0;
        
        for ( ;n < this.maxIterations; n++) {
            if (z1.magnitude() > this.tolerance ) {
                reason = Reason.OUTSIDE;
                break;
            }

            z1 = z.multiply(z).subtract(1).divide(z.multiply(z).subtract(1));
            derivate = z.multiply(2).multiply(z1);
            z = z.subtract(z1.divide(derivate));
        }
        
        return new FractalValue(n, c.asSeed(), z.asDistance(), derivate.asDerivate(), reason);
    }

    public double tolerance() {
        return this.tolerance;
    }

    public int maxIterations() {
        return this.maxIterations;
    }

}
