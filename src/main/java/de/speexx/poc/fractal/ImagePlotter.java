/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

import java.awt.image.BufferedImage;
import static java.lang.StrictMath.PI;
import static java.lang.StrictMath.log;

/**
 *
 * @author saschakohlmann
 */
public abstract class ImagePlotter {
    
    final static double HALF_PI = PI / 2;
    final static double LOG_OF_2 = log(2);

    public abstract BufferedImage draw(final Array2d<FractalValue> data);

}
