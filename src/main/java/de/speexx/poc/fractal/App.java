/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

import java.io.FileOutputStream;
import javax.imageio.ImageIO;
import static java.lang.StrictMath.ceil;

/**
 *
 * @author saschakohlmann
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        
        final var multiplier = 7;
        final var interpolation = 2;
        final var widthToHeightRatio = 1.33333333333333333333d;
        final var width = 600 * multiplier;
        final var height = (int) ceil(width / widthToHeightRatio);

        final var dimension = new Dimension(height, (int) width, interpolation);
        final var window = PlotWindow.PLOT_WINDOWS[7];
        final var config = new CreateConfig(dimension, window);
        
        System.out.format("start%n");
        final var mandelStart = System.nanoTime();
        final var formular = StandardMandelbrot.newInstance()
//                .withPixelSize(2)
//                .withThickness(1)
                .withEscape(5)
                .withZPower(2)
                .withMaximumIterations(window.maxIterations());
        final var array = new Mandelbrot().create(config, formular);
        final var mandelEnd = System.nanoTime();

        System.out.format("mandel: %d%n", ((mandelEnd - mandelStart) / 1_000_000));
        final var plotStart = System.nanoTime();
        final var image = new GradientImagePlotter(formular.maxIterations())
                .withSmooth(true)
                .withEscape(formular.escape())
//                .withEscape(formular.tolerance())
                .withLinearity(GradientImagePlotter.Linearity.TANGENS_HYPERBOLICUS)
                .withMaximumIterationColor(Colors.LIGHTGREEN)
                .withColorRange(4)
                .withReduceHalo(true)
                .withColorRepetitions(3)
                .withColors(Gradient.COLORS)
                .withCompress(false)
                .draw(array);

        final var plotEnd = System.nanoTime();
        System.out.format("plot: %d%n", ((plotEnd - plotStart) / 1_000_000));

        final var saveStart = System.nanoTime();
        try (final var output = new FileOutputStream(System.getProperty("user.home") + "/mandelbrot_00000.png")) {
            ImageIO.write(image, "PNG", output); 
        }
        final var saveEnd = System.nanoTime();
        System.out.format("save: %d%n", ((saveEnd - saveStart) / 1_000_000));
        System.out.format("total: %d%n", ((saveEnd - mandelStart) / 1_000_000));
    }
}
