/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

import static java.lang.StrictMath.atan2;
import static java.lang.StrictMath.cos;
import static java.lang.StrictMath.exp;
import static java.lang.StrictMath.hypot;
import static java.lang.StrictMath.sqrt;
import static java.lang.StrictMath.sin;
import de.speexx.poc.fractal.Complex.Seed;
import de.speexx.poc.fractal.Complex.Derivate;
import de.speexx.poc.fractal.Complex.Distance;

/**
 *
 * @author saschakohlmann
 */
public sealed class Complex permits Seed, Derivate, Distance {
    
    public static final Complex ZERO = new Complex();
    public static final Complex ONE = new Complex(1);
    public static final Complex NEGONE = new Complex (-1);
    public static final Complex INFINITY = new Complex(Double.POSITIVE_INFINITY,  Double.POSITIVE_INFINITY);

    private final double real;
    private final double imaginary;
    
    private Complex() {
        this(0);
    }

    public Complex(final double real) {
        this(real, 0);
    }

    public Complex(final double real, final double imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }

    public Complex add(final Complex cn) {
        return new Complex(this.real + cn.real, this.imaginary + cn.imaginary);
    }


    public Complex subtract(final Complex cn) {
        return new Complex(this.real - cn.real, this.imaginary - cn.imaginary);
    }
    
    public Complex multiply(final Complex cn) {
        final var _re = this.real * cn.real - this.imaginary * cn.imaginary;
        final var _im = this.imaginary * cn.real + this.real * cn.imaginary;
        return new Complex(_re, _im);
    }

    public Complex pow(final double x) {
        final var modulus = hypot(this.real, this.imaginary);
        final var arg = atan2(this.imaginary, this.real);
        final var logre = StrictMath.log(modulus);
        final var logim = arg;
        final var xlogre = x * logre;
        final var xlogim = x * logim;
        final var modulusans = exp(xlogre);
        return new Complex(modulusans * cos(xlogim), modulusans * sin(xlogim));
    }
    
    public Complex divide(final Complex cn) {
        // a+bi / c+di
        final var cAndDSquared = cn.real * cn.real + cn.imaginary * cn.imaginary;
        final var _re = (this.real * cn.real + this.imaginary * cn.imaginary) / cAndDSquared;
        final var _im = (this.imaginary * cn.real - this.real * cn.imaginary) / cAndDSquared;
        return new Complex(_re, _im);
    }

    public Complex add(final double scalar) {
        return this.add(new Complex(scalar));
    }

    public Complex subtract(final double scalar) {
        return this.subtract(new Complex(scalar));
    }

    public Complex multiply(final double scalar) {
        return this.multiply(new Complex(scalar));
    }

    public Complex divide(final double scalar) {
        return this.divide(new Complex(scalar));
    }
 
    public double abs2() {
        return this.real * this.real + this.imaginary * this.imaginary;
    }    

    public double rad2() {
        return abs2();
    }    
    
    public double r2() {
        return abs2();
    }    
    
    public double magnitude() {
        return hypot(this.real, this.imaginary);
    }

    public double r() {
        return magnitude();
    }

    public double rad() {
        return magnitude();
    }

    public double abs() {
        return magnitude();
    }    

    public double theta() {
        return atan2(this.imaginary, this.real);
    }   
    /**
     * Computes the conjugate of a complex number.
     */
    public Complex conjugate() {
        return new Complex(this.real, -1 * this.imaginary);
    }

    public static Complex polar(double r, double theta) {
        return new Complex(r * cos(theta), r * sin(theta));
    }

    public Complex exponential() {
        final var length = exp(this.real);
        return new Complex(length * cos(this.imaginary), length * sin(this.imaginary));
    }

    public Complex inverse() {
        final var length = this.real * this.real + this.imaginary * this.imaginary;
        return new Complex(this.real / length, -1 * this.imaginary / length);
    }

    public Complex log() {
        final var modulus = sqrt(this.real * this.real + this.imaginary * this.imaginary);
        final var arg = atan2(this.imaginary, this.real);
        return new Complex(StrictMath.log(modulus), arg);
    }

    public Complex sine() {
        return new Complex(sin(this.real) * /* cosh -> */ (exp(this.imaginary) + exp(-1 * this.imaginary)) / 2, cos(this.real) * /* sinh -> */ (exp(this.imaginary) - exp(-1 * this.imaginary)) / 2);
    }

    
    @Override
    public String toString() {
        return this.real + (this.imaginary < 0 ? "" : "+") + this.imaginary + "i";
    }
    
    public Derivate asDerivate() {
        return new de.speexx.poc.fractal.Complex.Derivate(this.real, this.imaginary);
    }

    public Seed asSeed() {
        return new de.speexx.poc.fractal.Complex.Seed(this.real, this.imaginary);
    }
    
    public Distance asDistance() {
        return new de.speexx.poc.fractal.Complex.Distance(this.real, this.imaginary);
    }
    
    public final static class Derivate extends Complex {
        Derivate(final double real, final double imaginary) {
            super(real, imaginary);
        }
    }
    
    public final static class Seed extends Complex {
        Seed(final double real, final double imaginary) {
            super(real, imaginary);
        }
    }
    
    public final static class Distance extends Complex {
        Distance(final double real, final double imaginary) {
            super(real, imaginary);
        }
    }
    
    public double real() {
        return this.real;
    }

    public double imaginary() {
        return this.imaginary;
    }
}
