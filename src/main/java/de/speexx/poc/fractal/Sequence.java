/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

import java.util.ArrayList;
import java.util.List;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import static java.lang.StrictMath.abs;

/**
 * 
 * @author saschakohlmann
 */
public class Sequence {
    
    private static final Logger LOG = System.getLogger(Sequence.class.getName());
    private static final Level LEVEL = Level.INFO;

    private final PlotWindow from;
    private final PlotWindow to;
    
    public Sequence(final PlotWindow from, final PlotWindow to) {
        this.from = from;
        this.to = to;
    }
    
    public List<PlotWindow> stepSequence(final int steps) {
        final List<PlotWindow> stepSequence = new ArrayList<>(steps);

        final var xswidth = abs(this.from().start().real() - to().start().real());
        final var ysheight = abs(this.from().start().imaginary() - to().start().imaginary());
        final var xswidthStep = xswidth / (double) steps;
        final var ysheightStep = ysheight / (double) steps;

        final var xewidth = abs(this.from().end().real() - to().end().real());
        final var yeheight = abs(this.from().end().imaginary() - to().end().imaginary());
        final var xewidthStep = xewidth / (double) steps;
        final var yeheightStep = yeheight / (double) steps;
        
        System.out.format("from: %s%n", from());
        System.out.format(" xsw: %f - ysh: %f - startReal+xw: %f - startIm+xh: %f%n", xswidth, ysheight, this.from().start().real() + xswidth, this.from().start().imaginary() + ysheight);
        System.out.format("  to: %s%n", to());
        System.out.format(" xew: %f - yeh: %f - startReal+xw: %f - startIm+xh: %f%n", xewidth, yeheight, this.from().end().real() - xewidth, this.from().end().imaginary() - yeheight);
        
        stepSequence.add(from());
        for (int i = 1; i < steps; i++) {
            final var newWindowStart = new Complex(this.from().start().real() + (xswidthStep * (double) i), this.from().start().imaginary() + (ysheightStep * (double) i));
            final var newWindowEnd = new Complex(this.from().end().real() - (xewidthStep * (double) i), this.from().end().imaginary() - (yeheightStep * (double) i));
            final var newPlotWindow = new PlotWindow(from.maxIterations(), newWindowStart, newWindowEnd);
            stepSequence.add(newPlotWindow);
        }
        stepSequence.add(to());
        
        return stepSequence;
    }

    public PlotWindow from() {
        return from;
    }

    public PlotWindow to() {
        return to;
    }
}
