/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

import static de.speexx.poc.fractal.Assertions.assertNotNull;
import static de.speexx.poc.fractal.Assertions.assertRange;
import static java.lang.String.format;
import java.util.Objects;

/**
 *
 * @author saschakohlmann
 */
public class ThicknessMandelbrot implements Mandelbrot.MandelbrotFormular {
    
    private final Mandelbrot.EscapeStrategy escapeStrategy;
    private final double zPower;
    private final double escape;
    private final int maxIterations;
    private final double thickness;
    private final double pixelSize;

    public static ThicknessMandelbrot newInstance() {
        return new ThicknessMandelbrot(Mandelbrot.EscapeStrategy.CONVERGENT, 2, 2, 80, 1, 1);
    }

    private ThicknessMandelbrot(final Mandelbrot.EscapeStrategy strategy, final double zPower, final double escape, final int maxIterations, final double thickness, final double pixelSize) {
        this.escapeStrategy = assertNotNull(strategy, () -> "Escape strategy must not be null");
        this.escape = assertRange(escape, 0, 100, () -> format("Escape must be between %f and %f. Is: %f", 0, 100, escape));
        this.zPower = assertRange(zPower, 0.000001, 100, () -> format("Z Power must be between %f and %f. Is: %f", 0.000001, 100, zPower));
        this.maxIterations = assertRange(maxIterations, 1, Integer.MAX_VALUE, () -> format("Maximum iterations must be between %d and %d. Is: %d", 1, Integer.MAX_VALUE, maxIterations));
        this.thickness = assertRange(thickness, 0.00000001, 60_000, () -> format("Thickness must be between %f and %f. Is: %f", 0.00000001, 60_000d, thickness));
        this.pixelSize = assertRange(pixelSize, 1d, 1_000d, () -> format("Pixel size must be between %f and %f. Is: %f", 1d, 1_000d, pixelSize));
    }

    @Override
    public FractalValue calculate(final Complex c) {
        var z = Complex.ZERO;
        var dc = Complex.ONE.multiply(this.thickness * this.pixelSize);
        var derivate = dc;
        var n = 0;
        var reason = Reason.NOT_ENOUGH_ITERATES;
        final var escape2 = this.escape * this.escape;

        if (this.zPower != 2.0) {
            for (n = 0; n < maxIterations; n++) {
                if (z.abs2() > derivate.abs2()) {
                    reason = Reason.BOUNDARY;
                    break;
                }
                if (z.abs2() > escape2 && n < this.maxIterations) {
                    reason = Reason.OUTSIDE;
                    break;
                }
                final var new_z = c.add(z.pow(this.zPower));
                final var new_derivate = derivate.multiply(2).multiply(z).add(dc);
                z = new_z;
                derivate = new_derivate;
             }
        } else {
            for (n = 0; n < this.maxIterations; n++) {
                if (z.abs2() > derivate.abs2()) {
                    reason = Reason.BOUNDARY;
                    break;
                }
                if (z.abs2() > escape2 && n < this.maxIterations) {
                    reason = Reason.OUTSIDE;
                    break;
                }
                final var new_z = c.add(z.multiply(z));
                final var new_derivate = derivate.multiply(2).multiply(z).add(dc);
                z = new_z;
                derivate = new_derivate;
            }
        }
        return new FractalValue(n, c.asSeed(), z.asDistance(), derivate.asDerivate(), reason);
    }

//        public final StandardMandelbrot withEscapeStrategy(final EscapeStrategy strategy) {
//            return new StandardMandelbrot(strategy, this.zPower, this.escape, this.maxIterations);
//        }

    /**
     * @param escape the escape value. Standard is 2 for Mandelbrot fractal. For better color smoothing the value can be higher.
     */
    public final ThicknessMandelbrot withEscape(final double escape) {
        return new ThicknessMandelbrot(this.escapeStrategy, this.zPower, escape, this.maxIterations, this.thickness, this.pixelSize);
    }

    /**
     * @param zPower the power of value. Must be between 0 and 16. Default should be 2 for standard Mandelbrot fractal.
     */
    public final ThicknessMandelbrot withZPower(final double zPower) {
        return new ThicknessMandelbrot(this.escapeStrategy, zPower, this.escape, this.maxIterations, this.thickness, this.pixelSize);
    }

    /**
     * @param maxIterations the maximum iterations for escape.
     */
    public final ThicknessMandelbrot withMaximumIterations(final int maxIterations) {
        return new ThicknessMandelbrot(this.escapeStrategy, this.zPower, this.escape, maxIterations, this.thickness, this.pixelSize);
    }

    /**
     * @param thickness the thickness.
     */
    public final ThicknessMandelbrot withThickness(final double thickness) {
        return new ThicknessMandelbrot(this.escapeStrategy, this.zPower, this.escape, this.maxIterations, thickness, this.pixelSize);
    }

    
    /**
     * @param thickness the thickness.
     */
    public final ThicknessMandelbrot withPixelSize(final double pixelSize) {
        return new ThicknessMandelbrot(this.escapeStrategy, this.zPower, this.escape, this.maxIterations, this.thickness, pixelSize);
    }

//        public final EscapeStrategy escapeStrategy() {
//            return this.escapeStrategy;
//        }

    public final double zPower() {
        return this.zPower;
    }

    public final double escape() {
        return this.escape;
    }

    public final int maxIterations() {
        return this.maxIterations;
    }

    public final double thickness() {
        return thickness;
    }

    public final double pixelSize() {
        return pixelSize;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.escapeStrategy);
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.zPower) ^ (Double.doubleToLongBits(this.zPower) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.escape) ^ (Double.doubleToLongBits(this.escape) >>> 32));
        hash = 97 * hash + this.maxIterations;
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.thickness) ^ (Double.doubleToLongBits(this.thickness) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.pixelSize) ^ (Double.doubleToLongBits(this.pixelSize) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ThicknessMandelbrot other = (ThicknessMandelbrot) obj;
        if (Double.doubleToLongBits(this.zPower) != Double.doubleToLongBits(other.zPower)) {
            return false;
        }
        if (Double.doubleToLongBits(this.escape) != Double.doubleToLongBits(other.escape)) {
            return false;
        }
        if (this.maxIterations != other.maxIterations) {
            return false;
        }
        if (Double.doubleToLongBits(this.thickness) != Double.doubleToLongBits(other.thickness)) {
            return false;
        }
        if (Double.doubleToLongBits(this.pixelSize) != Double.doubleToLongBits(other.pixelSize)) {
            return false;
        }
        return this.escapeStrategy == other.escapeStrategy;
    }

}
