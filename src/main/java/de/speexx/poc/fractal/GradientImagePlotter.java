/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

import java.awt.image.BufferedImage;
import java.util.stream.IntStream;
import java.awt.Color;
import static java.awt.Color.RED;
import static java.awt.Color.GREEN;
import static java.awt.Color.BLUE;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import static java.lang.String.format;
import static java.lang.StrictMath.PI;
import static java.lang.StrictMath.ceil;
import static java.lang.StrictMath.log10;
import static java.lang.StrictMath.log;
import static java.lang.StrictMath.sin;
import static java.lang.StrictMath.pow;
import static java.lang.StrictMath.tanh;
import static java.lang.StrictMath.max;
import static java.lang.StrictMath.cos;

import static de.speexx.poc.fractal.Assertions.assertRange;
import static de.speexx.poc.fractal.Assertions.assertNotNull;
import static de.speexx.poc.fractal.Assertions.assertSize;
import static de.speexx.poc.fractal.Assertions.assertContainsNoNull;
import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author saschakohlmann
 */
public class GradientImagePlotter extends ImagePlotter {

    final boolean smooth = true;
    final double escape;
    final Linearity linearity;
    int maximumIterations;
    final Color[] colors;
    final Color maximumIterationColor;
    final double colorRange;
    final boolean reduceHalo = false;
    final int colorRepetitions;
    final boolean compress = true;
    
    public GradientImagePlotter(final int maximumIterations) {
        this(true, 2, Linearity.ADAPTIVE_LOGARITHM, maximumIterations, new Color[] {RED, GREEN, BLUE}, Color.BLACK, 2, false, 1, true);
    }

    private GradientImagePlotter(final boolean smooth, final double escape, final Linearity lineratity, final int maximumIterations, final Color[] colors, final Color maximumIterationColor, final double colorRange, final boolean reduceHalo, final int colorRepetitions, final boolean compress) {
        this.maximumIterations = assertRange(maximumIterations, 1, Integer.MAX_VALUE, () -> format("maximum iterations must be in range from %d to %d. Is: %d", 0, Integer.MAX_VALUE, maximumIterations));
        this.linearity = assertNotNull(lineratity, () -> format("Linearity must not be null"));
        this.escape = assertRange(escape, 1, 100, () -> format("Escape value must be between %f and %f. Is: %f", 1, 100, escape));
        this.maximumIterationColor = assertNotNull(maximumIterationColor, () -> format("Maximum iteration color must not be null"));
        this.colorRange = assertRange(colorRange, 1, 10, () -> format("Color range must be between %f and %f. Is: %f", 1, 10, colorRange));
        this.colorRepetitions = assertRange(colorRepetitions, 1, Integer.MAX_VALUE, () -> format("Color repetitions must be in range from %d to %d. Is: %d", 0, Integer.MAX_VALUE, colorRepetitions));
        final var colorList = Arrays.asList(colors);
        assertContainsNoNull(colorList, () -> format("Gradient color must not contain null value"));
        assertSize(colorList, 1, 256, () -> format("Gradient color amount must be between %d and %d. Is: %d", 1, 256, colors.length));
        this.colors = colorList.toArray(new Color[colors.length]);
    }
    
    @Override
    public BufferedImage draw(final Array2d<FractalValue> data) {
        
        // To speed up things and don't do indirect lookups during calculation I invest in precalculation.
        final var colorRangeInverse = 1 / max(this.colorRange(), 0.0001);
        final var colorRangeTanH = tanh(this.colorRange());

        final var logMultiplier = pow(4, this.colorRange());
        final var logDivisor = log(logMultiplier + 1);

        final var defaultColor = this.maximumIterationColor();
        final var colorCount = (double) this.colors.length;
        final var multicolor = colorCount > 1;
        final var interpolation = data.dimension().interpolation();
        
        // Linas: Smooth Shading for the Mandelbrot Exterior - http://linas.org/art-gallery/escape/smooth.html
        final var minMaxIterations = minMaxFromMandelValueArray(data, this.maximumIterations());
        final var minIterations = this.smooth ? minMaxIterations.min().iterations() + 2d - log(log(minMaxIterations.min().distance().magnitude())) / LOG_OF_2 : minMaxIterations.min().iterations();
        final var maxIterations = this.smooth ? minMaxIterations.max().iterations() + 2d - log(log(minMaxIterations.max().distance().magnitude())) / LOG_OF_2 : minMaxIterations.max().iterations();
        
        // Color table with separate red, green and blue values of the gradient is faster in calculation
        // Length of array otimized for "Determine gradient color". See there below
        final var gradientColorsRed =  new double[this.colors.length][2];
        final var gradientColorsGreen =  new double[this.colors.length][2];
        final var gradientColorsBlue =  new double[this.colors.length][2];
        IntStream.range(0, this.colors.length-1).forEach(idx -> {
            gradientColorsRed[idx][0] = (double) this.colors[idx].getRed();
            gradientColorsRed[idx][1] = (double) this.colors[idx+1].getRed();
            gradientColorsGreen[idx][0] = (double) this.colors[idx].getGreen();
            gradientColorsGreen[idx][1] = (double) this.colors[idx+1].getGreen();
            gradientColorsBlue[idx][0] = (double) this.colors[idx].getBlue();
            gradientColorsBlue[idx][1] = (double) this.colors[idx+1].getBlue();
        });

        final var defaultColorRed = defaultColor.getRed();
        final var defaultColorGreen = defaultColor.getGreen();
        final var defaultColorBlue = defaultColor.getBlue();
        // Finish precalculation

        final var bufferedImage = new BufferedImage(data.dimension().columns(), data.dimension().rows(), TYPE_INT_ARGB);
        IntStream.range(0, data.dimension().columns()).parallel().forEach(x -> {
            IntStream.range(0, data.dimension().rows()).forEach(y -> {
                var red = 0.0;
                var green = 0.0;
                var blue = 0.0;
                var pixelCount = 0.0; // Interpolation Count

                for (int xx = 0; xx < interpolation; xx++) {
                    final var xxx = x * interpolation + xx;
                    for (int yy = 0; yy < interpolation; yy++){
                        pixelCount++;
                        final var yyy = y * interpolation + yy;
                        final var value = data.get(xxx, yyy);

                        final var iterations = value.iterations();
                        if (iterations < maxIterations) {
                            final var normalizedIterations = !this.smooth ? iterations : iterations + 2d - log(log(value.distance().magnitude())) / LOG_OF_2;

                            var f = 0.5;
                            
                            // Stefan Bion - Color Mapping - https://www.stefanbion.de/fraktal-generator/colormapping/index.htm
                            final double iterationDelta;
                            final double iterationIndex;
                            if (this.compress) {
                                iterationDelta = maxIterations - minIterations;
                                iterationIndex = normalizedIterations - minIterations;
                            } else {
                                iterationDelta = this.maximumIterations - 1;
                                iterationIndex = maxIterations - 1;
                            }
                                
                            if (iterationDelta > 1) {
                                if (multicolor) {
                                    f = (ceil(iterationIndex) % colorCount) * (iterationDelta - 1) / (colorCount - 1) / iterationDelta;
                                } else {
                                    f = iterationIndex / iterationDelta;
                                }
                                
                                // Calculate the gradient base of colors using different algorithms.
                                if (this.linearity == Linearity.ADAPTIVE_LOGARITHM) {
                                    f = (log(normalizedIterations) - log(minIterations)) / (log(maxIterations) - log(minIterations));
                                } else {
                                    f = (normalizedIterations - minIterations) / (maxIterations - minIterations);
                                    f = switch (this.linearity) {
                                        case QUARTER_SINE -> sin(f * HALF_PI);
                                        case LOGARITHM -> log(f * logMultiplier + 1) / logDivisor;
                                        case GAMMA -> pow(f, colorRangeInverse);
                                        case TANGENS_HYPERBOLICUS -> tanh(f * colorRange) / colorRangeTanH;
                                        default -> f;
                                    };
                                }

                                // Handle repitions
                                if (this.colorRepetitions > 1) {
                                    f = (f * this.colorRepetitions) % 1;
                                }
                            }

                            // Determine gradient color
                            f = f * (this.colors.length - 1);
                            final var v = ((int) ceil(f)) == 0 ? 0 : (int) (ceil(f) - 1);
                            f = f - (double) v;

                                // Handle halos - https://www.stefanbion.de/fraktal-generator/colormapping/index.htm
                            if (this.reduceHalo) {
                                f = (cos(f * PI + PI) + 1) / 2;
                            }

                            // Calculate gradient
                            red = red + (gradientColorsRed[v][0] + (gradientColorsRed[v][1] - gradientColorsRed[v][0]) * f);
                            green = green + (gradientColorsGreen[v][0] + (gradientColorsGreen[v][1] - gradientColorsGreen[v][0]) * f);
                            blue = blue + (gradientColorsBlue[v][0] + (gradientColorsBlue[v][1] - gradientColorsBlue[v][0]) * f);

                        } else {
                            red = (double) defaultColorRed;
                            green = (double) defaultColorGreen;
                            blue = (double) defaultColorBlue;
                        }
                    }
                }
                
                // Finish interpolation
                final var redi = (int) ceil(red / pixelCount);
                final var greeni = (int) ceil(green / pixelCount);
                final var bluei = (int) ceil(blue / pixelCount);
                
                // Gradient calculates values less than 0 and greater than 255. These values must be adjusted from 0 and 255 respectively.
                bufferedImage.setRGB(x, y, new Color(redi < 0 ? 0 : redi > 255 ? 255 : redi, greeni < 0 ? 0 : greeni > 255 ? 255 : greeni,  bluei < 0 ? 0 : bluei > 255 ? 255 : bluei).getRGB());
            });
        });
        
        
        return bufferedImage;
    }
    
    public final GradientImagePlotter withSmooth(final boolean smoothness) {
        return new GradientImagePlotter(smoothness, this.escape, this.linearity, this.maximumIterations, this.colors, this.maximumIterationColor, this.colorRange, this.reduceHalo, this.colorRepetitions, this.compress);
    }

    public final GradientImagePlotter withLinearity(final Linearity lin) {
        return new GradientImagePlotter(this.smooth, this.escape, lin, this.maximumIterations, this.colors, this.maximumIterationColor, this.colorRange, this.reduceHalo, this.colorRepetitions, this.compress);
    }

    public final GradientImagePlotter withMaximumIterationColor(final Color maxIterationColor) {
        return new GradientImagePlotter(this.smooth, this.escape, this.linearity, this.maximumIterations, this.colors, maxIterationColor, this.colorRange, this.reduceHalo, this.colorRepetitions, this.compress);
    }

    public final GradientImagePlotter withColorRange(final double colRange) {
        return new GradientImagePlotter(this.smooth, this.escape, this.linearity, this.maximumIterations, this.colors, this.maximumIterationColor, colRange, this.reduceHalo, this.colorRepetitions, this.compress);
    }

    public final GradientImagePlotter withReduceHalo(final boolean deHal) {
        return new GradientImagePlotter(this.smooth, this.escape, this.linearity, this.maximumIterations, this.colors, this.maximumIterationColor, this.colorRange, deHal, this.colorRepetitions, this.compress);
    }

    public final GradientImagePlotter withColorRepetitions(final int reps) {
        return new GradientImagePlotter(this.smooth, this.escape, this.linearity, this.maximumIterations, this.colors, this.maximumIterationColor, this.colorRange, this.reduceHalo, reps, this.compress);
    }

//    public final GradientImagePlotter withMaximumIterations(final int maxIterations) {
//        return new GradientImagePlotter(this.smooth, this.escape, this.linearity, maxIterations, this.colors, this.maximumIterationColor, this.colorRange, this.reduceHalo, this.colorRepetitions, this.compress);
//    }

    public final GradientImagePlotter withEscape(final double escapes) {
        return new GradientImagePlotter(this.smooth, escapes, this.linearity, this.maximumIterations, this.colors, this.maximumIterationColor, this.colorRange, this.reduceHalo, this.colorRepetitions, this.compress);
    }

    public final GradientImagePlotter withCompress(final boolean comp) {
        return new GradientImagePlotter(this.smooth, this.escape, this.linearity, this.maximumIterations, this.colors, this.maximumIterationColor, this.colorRange, this.reduceHalo, this.colorRepetitions, comp);
    }

    public final GradientImagePlotter withColors(final Color[] cols) {
        return new GradientImagePlotter(this.smooth, this.escape, this.linearity, this.maximumIterations, cols, this.maximumIterationColor, this.colorRange, this.reduceHalo, this.colorRepetitions, this.compress);
    }

    public final boolean isSmooth() {
        return this.smooth;
    }

    public final double escape() {
        return this.escape;
    }

    public final Linearity lineratity() {
        return this.linearity;
    }

    public final int maximumIterations() {
        return this.maximumIterations;
    }

    public final Color maximumIterationColor() {
        return this.maximumIterationColor;
    }

    public final double colorRange() {
        return this.colorRange;
    }

    public final boolean isReduceHalo() {
        return this.reduceHalo;
    }

    public final int colorRepetitions() {
        return this.colorRepetitions;
    }

    public final boolean isCompress() {
        return this.compress;
    }

    public final Color[] colors() {
        return Arrays.copyOf(this.colors, this.colors.length);
    }

    // Z.b. 
    static double logX(final double v, final double x) {
        return log10(v) / log10(x);
    }

    public enum Linearity {
        ADAPTIVE_LOGARITHM,
        LINEAR,
        QUARTER_SINE,
        LOGARITHM,
        GAMMA,
        TANGENS_HYPERBOLICUS,
    }
    
    static MinMax minMaxFromMandelValueArray(final Array2d<FractalValue> array, final int maximumIterations) {

        int minIterations = Integer.MAX_VALUE;
        int maxIterations = Integer.MIN_VALUE;
        FractalValue minMandel = null;
        FractalValue maxMandel = null;
        
        for (int x = 0; x < array.width(); x++) {
            for (int y = 0; y < array.height(); y++) {
                final var mandelValue = array.get(x, y);
                final var iters = mandelValue.iterations();
                if (iters < minIterations) {
                    minIterations = iters;
                    minMandel = mandelValue;
                }
                if (iters > maxIterations && iters < maximumIterations) {
                    maxIterations = iters;
                    maxMandel = mandelValue;
                }
            }
        }
        return new MinMax(minMandel, maxMandel);
    }
    
    static record MinMax(FractalValue min, FractalValue max) {
        public MinMax{
            if (max.iterations() < min.iterations()) {
                throw new IllegalArgumentException("min(" + min + ") must be lower or equal max(" + max + ")");
            }
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (this.smooth ? 1 : 0);
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.escape) ^ (Double.doubleToLongBits(this.escape) >>> 32));
        hash = 37 * hash + Objects.hashCode(this.linearity);
        hash = 37 * hash + this.maximumIterations;
        hash = 37 * hash + Arrays.deepHashCode(this.colors);
        hash = 37 * hash + Objects.hashCode(this.maximumIterationColor);
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.colorRange) ^ (Double.doubleToLongBits(this.colorRange) >>> 32));
        hash = 37 * hash + (this.reduceHalo ? 1 : 0);
        hash = 37 * hash + this.colorRepetitions;
        hash = 37 * hash + (this.compress ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GradientImagePlotter other = (GradientImagePlotter) obj;
        if (this.smooth != other.smooth) {
            return false;
        }
        if (Double.doubleToLongBits(this.escape) != Double.doubleToLongBits(other.escape)) {
            return false;
        }
        if (this.maximumIterations != other.maximumIterations) {
            return false;
        }
        if (Double.doubleToLongBits(this.colorRange) != Double.doubleToLongBits(other.colorRange)) {
            return false;
        }
        if (this.reduceHalo != other.reduceHalo) {
            return false;
        }
        if (this.colorRepetitions != other.colorRepetitions) {
            return false;
        }
        if (this.compress != other.compress) {
            return false;
        }
        if (this.linearity != other.linearity) {
            return false;
        }
        if (!Arrays.deepEquals(this.colors, other.colors)) {
            return false;
        }
        return Objects.equals(this.maximumIterationColor, other.maximumIterationColor);
    }
}
