/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

import static de.speexx.poc.fractal.Assertions.assertNotNull;
import static de.speexx.poc.fractal.Assertions.assertRange;
import static java.lang.String.format;
import java.util.Objects;

/**
 *
 * @author saschakohlmann
 */
public class StandardJuliaSet implements Mandelbrot.MandelbrotFormular {
    
    private final Mandelbrot.EscapeStrategy escapeStrategy;
    private final double zPower;
    private final double escape;
    private final int maxIterations;
    private final Complex constant;

    public static StandardJuliaSet newInstance() {
        return new StandardJuliaSet(Mandelbrot.EscapeStrategy.CONVERGENT, 2, 2, 80, new Complex(-0.79, 0.15));
//        return new StandardJuliaSet(Mandelbrot.EscapeStrategy.CONVERGENT, 2, 2, 80, new Complex(-0.7, 0.27015));
    }

    private StandardJuliaSet(final Mandelbrot.EscapeStrategy strategy, final double zPower, final double escape, final int maxIterations, final Complex constant) {
        this.escapeStrategy = assertNotNull(strategy, () -> "Escape strategy must not be null");
        this.escape = assertRange(escape, 0, 100, () -> format("Escape must be between %f and %f. Is: %f", 0, 100, escape));
        this.zPower = assertRange(zPower, 0.000001, 100, () -> format("Z Power must be between %f and %f. Is: %f", 0.000001, 100, zPower));
        this.maxIterations = assertRange(maxIterations, 1, Integer.MAX_VALUE, () -> format("Maximum iterations must be between %d and %d. Is: %d", 1, Integer.MAX_VALUE, maxIterations));
        this.constant = assertNotNull(constant, () -> format("Constant must not be null"));
    }

    @Override
    public FractalValue calculate(final Complex c) { 
       var z = c;
        var derivate = Complex.ONE;
        var n = 0;
        var reason = Reason.NOT_ENOUGH_ITERATES;
        final var escape2 = this.escape * this.escape;

        if (this.zPower != 2.0) {
            for ( ; n < this.maxIterations; n++) {
                if (z.abs2() > escape2 && n < this.maxIterations) {
                    reason = Reason.OUTSIDE;
                    break;
                }
                final var new_z = this.constant.add(z.pow(this.zPower));
                final var new_derivate = derivate.multiply(2).multiply(z).add(Complex.ONE);
                z = new_z;
                derivate = new_derivate;
             }
        } else {
            for ( ; n < this.maxIterations; n++) {
                if (z.abs2() > escape2 && n < this.maxIterations) {
                    reason = Reason.OUTSIDE;
                    break;
                }
                final var new_z = this.constant.add(z.multiply(z));
                final var new_derivate = derivate.multiply(2).multiply(z).add(Complex.ONE);
                z = new_z;
                derivate = new_derivate;
            }
        }
        return new FractalValue(n, c.asSeed(), z.asDistance(), derivate.asDerivate(), reason);
    }

//        public final StandardMandelbrot withEscapeStrategy(final EscapeStrategy strategy) {
//            return new StandardMandelbrot(strategy, this.zPower, this.escape, this.maxIterations);
//        }

    /**
     * @param escape the escape value. Standard is 2 for Mandelbrot fractal. For better color smoothing the value can be higher.
     */
    public final StandardJuliaSet withEscape(final double escape) {
        return new StandardJuliaSet(this.escapeStrategy, this.zPower, escape, this.maxIterations, this.constant);
    }

    /**
     * @param zPower the power of value. Must be between 0 and 16. Default should be 2 for standard Mandelbrot fractal.
     */
    public final StandardJuliaSet withZPower(final double zPower) {
        return new StandardJuliaSet(this.escapeStrategy, zPower, this.escape, this.maxIterations, this.constant);
    }

    /**
     * @param maxIterations the maximum iterations for escape.
     */
    public final StandardJuliaSet withMaximumIterations(final int maxIterations) {
        return new StandardJuliaSet(this.escapeStrategy, this.zPower, this.escape, maxIterations, this.constant);
    }

    public final StandardJuliaSet withConstant(final Complex constant) {
        return new StandardJuliaSet(this.escapeStrategy, this.zPower, this.escape, maxIterations, constant);
    }

//        public final EscapeStrategy escapeStrategy() {
//            return this.escapeStrategy;
//        }

    public final double zPower() {
        return this.zPower;
    }

    public final double escape() {
        return this.escape;
    }

    public final Complex constant() {
        return this.constant;
    }

    public final int maxIterations() {
        return this.maxIterations;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.escapeStrategy);
        hash = 11 * hash + (int) (Double.doubleToLongBits(this.zPower) ^ (Double.doubleToLongBits(this.zPower) >>> 32));
        hash = 11 * hash + (int) (Double.doubleToLongBits(this.escape) ^ (Double.doubleToLongBits(this.escape) >>> 32));
        hash = 11 * hash + this.maxIterations;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StandardJuliaSet other = (StandardJuliaSet) obj;
        if (Double.doubleToLongBits(this.zPower) != Double.doubleToLongBits(other.zPower)) {
            return false;
        }
        if (Double.doubleToLongBits(this.escape) != Double.doubleToLongBits(other.escape)) {
            return false;
        }
        if (this.maxIterations != other.maxIterations) {
            return false;
        }
        return this.escapeStrategy == other.escapeStrategy;
    }
}
