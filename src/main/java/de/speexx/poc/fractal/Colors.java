/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

import java.awt.Color;
import static java.lang.StrictMath.pow;
import static java.lang.StrictMath.sin;
import static java.lang.StrictMath.PI;

/**
 *
 * @author saschakohlmann
 */
public class Colors {

    final static Color LIGHTBLUE = new Color(0xbf, 0xca, 0xff);
    final static Color ELECTRICBLUE = new Color(0x7d, 0xf9, 0xff);
    final static Color NAVYDARKBLUE = new Color(0x00, 0x02, 0x2e);
    final static Color VERYDARKGRAY = new Color(20, 20, 20);
    final static Color DARKRED = new Color(0x8b, 0, 0);
    final static Color VERYDARKRED = new Color(0x60, 0, 0);
    final static Color DARKGREEN = new Color(0x00, 0x2a, 0x0b);
    final static Color VERYDARKGREEN = new Color(0x00, 0x1e, 0x0e);
    final static Color LEAFGREEN = new Color(0x00, 0x64, 0x00);
    final static Color LIGHTGREEN = new Color(0x99, 0xfa, 0x76);
    final static Color RICHRED = new Color(0xa1, 0x00, 0x0e);
    final static Color MEDIUMDARKRED = new Color(0x80, 0x05, 0x00);
    final static Color GRASSGREEN = new Color(0x00, 0x9a, 0x17);
    final static Color MEDIUMGREEN = new Color(0x00, 0x80, 0x13);
    
    final static Color CANNIEST_SEPIA = new Color(0x98, 0x5e, 0x2c);
    final static Color ILIAC_BRIGHT_VIOLET = new Color(0xad, 0x09, 0xfd);
    final static Color DELIGENT_LIGHT_NAVY = new Color(0x14, 0x50, 0x84);
    
    final static Color BLACK = Color.BLACK;
    final static Color BLUE = Color.BLUE;
    final static Color CYAN = Color.CYAN;
    final static Color DARK_GRAY = Color.DARK_GRAY;
    final static Color GRAY = Color.GRAY;
    final static Color GREEN = Color.GREEN;
    final static Color LIGHT_GRAY = Color.LIGHT_GRAY;
    final static Color MAGENTA = Color.MAGENTA;
    final static Color ORANGE = Color.ORANGE;
    final static Color PINK = Color.PINK;
    final static Color RED = Color.RED;
    final static Color WHITE = Color.WHITE;
    final static Color YELLOW = Color.YELLOW;
    
    public final static double PHI = (1 + pow(5, 0.5))  / 2;
    public final static double ONE_THIRD = 1d/3d;
    public final static double TWO_THIRD = 2d/3d;
    public final static double HALF_PI = PI / 2;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final var color = sinbow(120);
        System.out.format("color: %s%n", color);
    }
    
    
    public static Color nthColor(final double n) {
        return sinbow(n * PHI);
    }
    
    public static Color sinbow(final double hue) {
        final var halfAddedHue = hue + 0.5;
        final var negatedHue = halfAddedHue * -1;
        final var red = sin(PI * negatedHue);
        final var green = sin(PI * (negatedHue + ONE_THIRD));
        final var blue = sin(PI * (negatedHue + ONE_THIRD));
        
        return new Color((int) (255 * red * red), (int) (255 * green * green), (int) (255 * blue * blue));
    }
}
