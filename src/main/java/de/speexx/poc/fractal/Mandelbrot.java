/* This file is part of the SpeexX Fractal Something.
 * Copyright (C) 2022 Sascha Kohlmann
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package de.speexx.poc.fractal;

import java.util.stream.IntStream;

/**
 * @author saschakohlmann
 */
public class Mandelbrot {
    
    public Array2d<FractalValue> create(final CreateConfig config, final Formular form) {
        final var array = Array2d.newInstance(FractalValue.class, config.imageDimension());

        final var plotWindow = config.mandelbrotWindow().adjustAspectRatio(config.imageDimension());
        
        final var width = config.imageDimension().interpolatedColumns();
        final var height = config.imageDimension().interpolatedRows();

        final var startReal = plotWindow.start().real();
        final var startImaginary = plotWindow.start().imaginary();
        final var endReal = plotWindow.end().real();
        final var endImaginary = plotWindow.end().imaginary();

        var realStep = (endReal - startReal) / (double) width;
        var imaginaryStep = (endImaginary - startImaginary) / (double) height;
        
        IntStream.range(0, height).parallel().forEach(y -> {
            final var cy = endImaginary - y * imaginaryStep;
            IntStream.range(0, width).forEach(x -> {
                final var cx = startReal + x * realStep;
                final var c = new Complex(cx, cy);
                final var value = form.calculate(c);
                array.set(x, y, value);
            });
        });
        
        return array;
    }

    public static interface MandelbrotFormular extends Formular {}
    
    public enum EscapeStrategy {
        CONVERGENT,
        DIVERGENT
    }
}
